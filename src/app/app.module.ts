import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { RestauranteTableComponent } from '../restaurante-table/restaurante-table.component';
import { PratoTableComponent } from '../prato-table/prato-table.component';
import { RestauranteInputComponent } from '../restaurante-input/restaurante-input.component';
import { PratoInputComponent } from '../prato-input/prato-input.component';
import { HomeComponent } from '../home/home.component';
import { Http, HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const appRoutes: Routes = [
  {
    path: 'restaurante',
    component: RestauranteTableComponent
  },
  {
    path: 'prato',
    component: PratoTableComponent,
  },
  {
    path: 'restaurante/:acao/:id',
    component: RestauranteInputComponent,
  },
  {
    path: 'restaurante/:acao',
    component: RestauranteInputComponent,
  },
  {
    path: 'prato/:acao/:id',
    component: PratoInputComponent,
  },
  {
    path: 'prato/:acao',
    component: PratoInputComponent,
  },
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    RestauranteTableComponent,
    PratoTableComponent,
    RestauranteInputComponent,
    PratoInputComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    NgbModule.forRoot(),
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
