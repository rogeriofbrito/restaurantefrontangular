import {Component} from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import 'rxjs/add/operator/map'

@Component({
  selector: 'app-root',
  templateUrl: './prato-table.component.html'
})

export class PratoTableComponent {
    apiRoot: string = "http://localhost:8080";
    descricao: string = "";
    pagina: number = 1;
    itensPorPagina: number = 5;
    page = 0;
    textoPesquisa: string;
    
    pratos;
    collectionSize: number;

    constructor(
        private http: Http,
       private modalService: NgbModal 
    ) {
        this.getPratos(this.descricao, 0, this.itensPorPagina);
    }

    onPageChange($event) {
        this.getPratos(this.descricao, $event - 1, this.itensPorPagina);
    }

    itensPorPaginaChanged() {
        this.getPratos(this.descricao, 0, this.itensPorPagina);
    }

    pesquisaPratoClick() {
        this.getPratos(this.descricao, 0, this.itensPorPagina);
    }
    
    getPratos(descricao, pagina, itensPorPagina){
        let url = `${this.apiRoot}/prato/descricao=${descricao}/pagina=${pagina}/itensPorPagina=${itensPorPagina}`;
        this.http.get(url)
        .map((data) => data = data.json())
        .subscribe(
            (data) => {
                this.pratos = data.pratos; 
                this.collectionSize = data.total;
            },
            (err) => {
                console.error("Houve um erro ao buscar o registro. ");
            }
        );
    }

    deletePratos(idPrato){
        let url = `${this.apiRoot}/prato/id=${idPrato}`;
        this.http.delete(url)
        .subscribe(
            (data) => {
                console.log(data.status);
                this.getPratos(this.descricao, this.page - 1, this.itensPorPagina);
            },
            (err) => {
                console.error("Houve um erro ao excluir o registro: " + err.message);
            }
        );
    }

    open(content, idPrato) {
        this.modalService.open(content).result.then(
            (result) => {
                if(result == 's') {
                    this.deletePratos(idPrato);
                } else {
                    console.log("Resposta: não");
                }
            },
            (reason) => {
                console.log("Modal fechado. Nenhuma ação.");
            }
        );
    }
}