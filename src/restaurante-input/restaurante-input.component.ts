import { OnInit, Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions, Headers } from '@angular/http';

import { Restaurante } from '../models/restaurante'; 

@Component({
  selector: 'app-root',
  templateUrl: './restaurante-input.component.html'
})

export class RestauranteInputComponent implements OnInit {
    private apiRoot: string = "http://localhost:8080";
    private restaurante: Restaurante;

    //private routeId: string; //id recebido na url
    //private descricao: string; //bind com campo descrição

    //private restaurante: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private http: Http,
        private router: Router
    ) {
        this.restaurante = new Restaurante();
    }

    ngOnInit() {
        //Tratamento das opções cadastrar e editar da URL
        this.activatedRoute.params.subscribe((params: Params) => {
            if(params.acao == "cadastrar") {
                this.restaurante.idRestaurante = null;
                if(typeof params.id != "undefined") {
                    console.error("Rota não deve receber parâmetro id");
                }
            
            } else if(params.acao == "editar") {
                let url = `${this.apiRoot}/restaurante/id=${params.id}`;
                this.http.get(url)
                .map(data => data = data.json())
                .subscribe(
                    (data) => {
                        this.restaurante.idRestaurante = data.idRestaurante;
                        this.restaurante.descricao = data.descricao;
                    },
                    (err) => {
                        this.router.navigate(['restaurante']);
                        console.error("Não foi encontrado nenhum restaurante com o id passado!");
                    }
                );
            } else {
                console.error("Rota inválida");
                this.router.navigate(['restaurante']);
            }
        });
        
    }

    salvarRestauranteClick() {
        this.postRestaurante(<JSON>this.restaurante);
    }

    postRestaurante(restaurante: JSON) {
        let url = `${this.apiRoot}/restaurante`;
        this.http.post(url, restaurante)
        .map(data => data.json())
        .subscribe(
            (data) => {
                console.log(data);
                this.router.navigate(['restaurante']);
            },
            (err) => {
                console.error("Erro ao salvar restaurante!");
            }
        );
    }
}