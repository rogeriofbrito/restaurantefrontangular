export class Prato {
    constructor(
        public idPrato?: number,
        public idRestaurante?: number,
        public descricao?: string,
        public preco?: number
    ) {}
}