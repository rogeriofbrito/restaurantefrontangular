import { Component, NgModule } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './restaurante-table.component.html'
})

export class RestauranteTableComponent {
    apiRoot: string = "http://localhost:8080";
    descricao: string = "";
    pagina: number = 1;
    itensPorPagina: number = 5;
    page = 0;
    textoPesquisa: string;

    restaurantes;
    collectionSize: number;

    constructor(
        private http: Http,
        private modalService: NgbModal
    ) {
        this.getRestaurantes(this.descricao, 0, this.itensPorPagina);
    }
    
    onPageChange($event) {
        this.getRestaurantes(this.descricao, $event - 1, this.itensPorPagina);
    }

    itensPorPaginaChanged() {
        this.getRestaurantes(this.descricao, 0, this.itensPorPagina);
    }

    pesquisaRestauranteClick() {
        this.getRestaurantes(this.descricao, 0, this.itensPorPagina);
    }

    getRestaurantes(descricao, pagina, itensPorPagina){
        let url = `${this.apiRoot}/restaurante/descricao=${descricao}/pagina=${pagina}/itensPorPagina=${itensPorPagina}`;
        this.http.get(url)
        .map((data) => data = data.json())
        .subscribe(
            (data) => {
                this.restaurantes = data.restaurantes; 
                this.collectionSize = data.total;
            },
            (err) => {
                console.error("Houve obter restaurantes.");
            }
        );
    }

    deleteRestaurantes(idRestaurante){
        let url = `${this.apiRoot}/restaurante/id=${idRestaurante}`;
        this.http.delete(url)
        .subscribe(
            (data) => {
                console.log(data.status);
                this.getRestaurantes(this.descricao, this.page - 1, this.itensPorPagina);
            },
            (err) => {
                console.error("Houve um erro ao excluir o registro: " + err.message);
            }
        );
    }


    open(content, idRestaurante) {
        this.modalService.open(content).result.then(
            (result) => {
                if(result == 's') {
                    console.log(idRestaurante);
                    this.deleteRestaurantes(idRestaurante);
                } else {
                    console.log("Resposta: não");
                }
            },
            (reason) => {
                console.log("Modal fechado. Nenhuma ação.");
            }
        );
    }
}