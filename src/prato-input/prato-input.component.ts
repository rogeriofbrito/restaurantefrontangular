import { OnInit, Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map'

import { Prato } from '../models/prato';

@Component({
  selector: 'app-root',
  templateUrl: './prato-input.component.html'
})

export class PratoInputComponent implements OnInit {
    
    private apiRoot: string = "http://localhost:8080";
    private prato: Prato;
    private restaurantes: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private http: Http,
        private router: Router
    ) {
        this.prato = new Prato();
    }

    ngOnInit() {
        //Requisição para receber objetos para montar select de restaurantes
        let url = `${this.apiRoot}/restaurante/all`;
        this.http.get(url)
        .map(data => data = data.json())
        .subscribe(
            (data) => {
                this.restaurantes = data;
            },
            (err) => {
                console.error("Erro ao obter restaurentes para montar select!")
            }
        );

        //Tratamento das opções cadastrar e editar da URL
        this.activatedRoute.params.subscribe((params: Params) => {
            if(params.acao == "cadastrar") {
                this.prato.idPrato = null;
                if(typeof params.id != "undefined") {
                    console.error("Rota não deve receber parâmetro id");
                }

            } else if(params.acao == "editar") {
                let url = `${this.apiRoot}/prato/id=${params.id}`;
                this.http.get(url)
                .map(data => data = data.json())
                .subscribe(
                    (data) => {
                        this.prato.idPrato = data.idPrato;
                        this.prato.idRestaurante = data.restaurante.idRestaurante;
                        this.prato.descricao = data.descricao;
                        this.prato.preco = data.preco;
                    },
                    (err) => {
                        this.router.navigate(['prato']);
                        console.error("Não foi encontrado nenhum prato com o id passado!");
                    }
                );

            } else {
                console.error("Rota inválida");
                this.router.navigate(['prato']);
            }
        });
    }

    salvarPratoClick() {
        this.prato.idRestaurante = +this.prato.idRestaurante;
        this.postPrato(<JSON>this.prato);
    }

    postPrato(prato: JSON) {
        let url = `${this.apiRoot}/prato`;
        this.http.post(url, prato)
        .map(data => data.json())
        .subscribe(
            (data) => {
                console.log(data);
                this.router.navigate(['prato']);
            },
            (err) => {
                console.error("Erro ao salvar prato!");
            }
        );
    }
}